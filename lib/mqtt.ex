defmodule MQTT do
    use Application

    def start(_type, _args) do
        pool_config = [mfa: {MQTT.Worker, :start_link, []}, size: 5]
        MQTT.Supervisor.start_link(pool_config)
    end
end