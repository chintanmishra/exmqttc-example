defmodule MQTT.Supervisor do
    use Supervisor

    ##############
    # Client API #
    ##############
    def start_link(pool_config) do
        Supervisor.start_link(__MODULE__, pool_config, name: __MODULE__)
    end

    #############
    # Callbacks #
    #############
    def init(pool_config) do
        children = [
            {MQTT.Server, [self(), pool_config]}
        ]

        opts = [
            strategy: :one_for_all
        ]
        Supervisor.init(children, opts)
    end
end