defmodule MQTT.Server do
    use GenServer

    defmodule State do
        defstruct sup: nil, size: nil, mfa: nil, worker_sup: nil, workers: nil
    end

    ##############
    # Client API #
    ##############
    def start_link([sup, pool_config]) do
        GenServer.start_link(__MODULE__, [[sup, pool_config]], name: __MODULE__)
    end

    #############
    # Callbacks #
    #############
    def init([[sup, pool_config]]) when is_pid(sup) do
        # monitors = :ets.new(:monitors, [:private])
        init(pool_config, %State{sup: sup})
    end

    def init([{:mfa, mfa} | rest], state) do
        init(rest, %{state | mfa: mfa})
    end

    def init([{:size, size} | rest], state) do
        init(rest, %{state | size: size})
    end

    def init([_ | rest], state) do
        init(rest, state)
    end

    def init([], state) do
        send self(), :start_worker_supervisor
        {:ok, state}
    end
    
    def handle_info(:start_worker_supervisor, state = %{mfa: mfa, size: size, sup: sup}) do
        supervisor_spec = %{
            id: MQTT.WorkerSupervisor,
            start: {MQTT.WorkerSupervisor, :start_link, [mfa]},
            restart: :temporary,
            type: :supervisor
        }

        {:ok, worker_sup} = Supervisor.start_child(sup, supervisor_spec)

        workers = prepopulate(size, worker_sup)
        {:noreply, %{state | workers: workers, worker_sup: worker_sup}}
    end

    ####################
    # Helper Functions #
    ####################
    defp prepopulate(size, sup) do
        prepopulate(size, sup, [])
    end

    defp prepopulate(size, _sup, workers) when size < 1 do
        workers
    end

    defp prepopulate(size, sup, workers) do
        prepopulate(size - 1, sup, [new_worker(sup) | workers])
    end

    defp new_worker(sup) do
        {:ok, worker} = Supervisor.start_child(sup, [[]])
        worker
    end
end