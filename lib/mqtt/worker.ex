defmodule MQTT.Worker do
    use GenServer

    defmodule MQTTOpt do
        defstruct mqtt_pid: nil
    end

    ##############
    # Client API #
    ##############
    def start_link([]) do
        GenServer.start_link(__MODULE__, :ok, [])
    end

    def sub(pid) do
        GenServer.call(pid, :sub)
    end

    def pub(pid) do
        GenServer.call(pid, :pub)
    end

    def get_state(pid) do
        GenServer.call(pid, :state)
    end

    def end_conn(pid) do
        GenServer.call(pid, :disconnect)
    end

    #############
    # Callbacks #
    #############
    def init(:ok) do
        {:ok, pid} = Exmqttc.start_link(MQTT.Callback, [name: :client], host: '127.0.0.1')
        MQTT.Worker.sub(pid)
        {:ok, %MQTTOpt{mqtt_pid: pid}}
    end

    def handle_call(:sub, _from, %{mqtt_pid: pid} = state) do
        MQTT.Worker.subscribe(pid)
        :timer.sleep(500)
        MQTT.Worker.pub(pid)
        :timer.sleep(500)
        {:reply, state, state}
    end

    def handle_call(:pub, _from, %{mqtt_pid: pid} = state) do
        MQTT.Worker.publish(pid)
        {:reply, state, state}
    end

    def handle_call(:state, _from, state) do
        IO.puts state
        {:reply, state, state}
    end

    def handle_call(:disconnect, _from, %{mqtt_pid: pid} = state) do
        Exmqttc.disconnect(pid)
        {:reply, "", state}
    end
    
    ####################
    # Helper Functions #
    ####################
    def publish(pid) do
        Exmqttc.publish(pid, "test", "elixir's payload")
    end

    def subscribe(pid) do
        Exmqttc.subscribe(pid, "test")
    end
end