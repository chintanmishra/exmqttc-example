defmodule MQTT.WorkerSupervisor do
    use Supervisor

    ##############
    # Client API #
    ##############
    def start_link({_, _, _} = mfa) do
        # mfa = module-function-arguments
        Supervisor.start_link(__MODULE__, mfa)
    end

    #############
    # Callbacks #
    #############
    def init({m, f, a}) do
        worker_opts = [
            restart: :temporary,
            function: f
        ]

        children = [
            {m, [a, worker_opts]}
        ]

        opts = [
            strategy: :simple_one_for_one,
            max_restarts: 5,
            max_seconds: 5
        ]

        Supervisor.start_link(children, opts)
    end
    
end