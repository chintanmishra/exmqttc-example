defmodule MQTT.Callback do
    require Logger
    use Exmqttc.Callback

    def init() do
        {:ok, []}
    end
    
    def handle_connect(state) do
        Logger.debug("Connected")
        {:ok, state}
    end
    
    def handle_disconnect(state) do
        Logger.debug("Disconnected")
        {:ok, state}
    end

    def handle_publish(topic, message, state) do
        IO.puts state
        Logger.debug "Message received on topic #{topic} with payload #{message}"
        {:ok, state}
    end

    # Add helper function to deal with the message
end